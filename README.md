# Confluence User Macros #

This repoisotry contains some Atlassian Confluence User Macros which might help some users to solve problems or enhance Confluence with some functions not available out-of-the-box.

### How do I get set up? ###
* Only for Confluence Server
* Needs Sysadmin privileges to install
* Open Administration -> User Macros
* Create a new macro for each vm-file inside the repo
* Copy & paste vm-file in template field
* Add name, title,...
* Choose: No macro body
* Save macro
* Create a new page & insert macro

Check out: https://confluence.atlassian.com/doc/writing-user-macros-4485.html